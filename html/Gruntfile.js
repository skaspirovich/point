module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		less: {
			development: {
				options: {
					paths: ["css"]
				},
				files: {
					"css/result.css": ["less/source.less", "less/media.less"]
				}
			}
		},

		cssmin: {
			combine: {
				files: {
					'css/production.min.css': [
						'css/bootstrap.min.css',
						'css/result.css',
                        'css/animations.css'
					]
				}
			}
		},

		watch: {
			css: {
				files: ['less/*.less'],
				tasks: ['less', 'cssmin']
			}
		}

	});

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['less', 'cssmin']);

};