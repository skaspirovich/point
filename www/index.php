<?php

// Предполагается, что websev делает get запрос
$fileUrl = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

if(!empty($fileUrl)){

    $url    = parse_url($fileUrl);
    $file   = $url["path"];

    if(file_exists($file)){

        $fp         = fopen($file, 'rb');

        // Генерируем уникальный ключ. Он будет выступать в роли виртуального URL.
        $fileName   = uniqid().".txt";

        // ВЫСТАВЛЯЕМ ЗАГОЛОВКИ
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Length: ' . filesize($file));
        header("Content-Type: text/plain");

        // Дата последней модификации
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

        // HTTP 1.1
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        // HTTP 1.0
        header('Pragma: no-cache');

        // Отдаем файл
        fpassthru($fp);

        exit;
    }

}